using System.Collections.Generic;

namespace Deaven.IntValueTracker.Behaviours
{
    public interface IIntObjectCondition
    {
        public void CheckCondition(int value, List<IntValueTracker.ActionPoint> points);
    }
}