using System;
using System.Collections.Generic;
using Deaven.IntValueTracker.Behaviours;
using KristinaWaldt.ValueObjects;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Deaven.IntValueTracker
{
    [CreateAssetMenu(fileName = "IntValueTracker", menuName = "IntValueTracker/Tracker", order = 0)]
    public class IntValueTracker : SerializedScriptableObject
    {
        [Serializable]
        public class ActionPoint
        {
            public int value;
            public UnityEvent OnPointReached;
        }
        
        [SerializeField] private IntObject intValue;
        [SerializeField] private IIntObjectCondition condition;
        [SerializeField] private List<ActionPoint> actionPoints;
        
        private void OnEnable()
        {
            if (!intValue)
                return;
        
            intValue.ValueChangedTo += OnValueChanged;
        }
        
        private void OnDisable()
        {
            if (!intValue)
                return;
        
            intValue.ValueChangedTo -= OnValueChanged;
        }
        
        private void OnValueChanged(int value)
        {
            condition.CheckCondition(value, actionPoints);
        }
    }
}