using System.Collections.Generic;
using Deaven.IntValueTracker.Behaviours;

namespace Deaven.IntValueTracker.Implementations
{
    public class LessEqual : IIntObjectCondition
    {
        public void CheckCondition(int value, List<IntValueTracker.ActionPoint> points)
        {
            foreach (IntValueTracker.ActionPoint point in points)
            {
                if (value <= point.value)
                    point.OnPointReached?.Invoke();
            }
        }
    }
}